exports.calculator = function (variabel_1, variabel_2, operator) {
  let result
  switch (operator) {
    case 'plus':
      result = variabel_1 + variabel_2
      break

    case 'min':
      result = variabel_1 - variabel_2
      break

    case 'mult':
      result = variabel_1 * variabel_2
      break

    case 'div':
      result = variabel_1 / variabel_2
      break

    default:
      return 'Salah operator!'
  }
}
