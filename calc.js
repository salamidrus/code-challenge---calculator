let var1;
let var2;

function multiple(var1, var2){
    console.log(var1*var2);
    return var1*var2;
}

function divide(var1, var2){
    console.log(var1/var2);
    return var1/var2;
}

function substract(var1,var2){
    console.log(var1-var2);
    return var1-var2;
}

function plus(var1,var2){
    console.log(var1+var2);
    return var1+var2;
}

exports.multiple = function (var1,var2){
    return var1 * var2;
}


exports.divide = function (var1,var2){
    return var1 / var2;
}


exports.substract = function (var1,var2){
    return var1 - var2;
}


exports.plus = function (var1,var2){
    return var1 + var2;
}

/*
module.exports.plus = (var1, var2) => {
    return a/b;
}
*/